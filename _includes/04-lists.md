## Leisure Activities

* Programming/Coding
  * Currently, These are the repositories am working on.:point_down:
  1. [Markdown Proforlio](https://github.com/VictorOmondi1997/markdown-portfolio)
  2. [Tutomalix Education Website](https://github.com/VictorOmondi1997/TutomalixEducation-Website)
  3. [Vick Ventures](https://github.com/VictorOmondi1997/Web_Design_Practicals)
  4. [Victor Omondi Website](https://github.com/VictorOmondi1997/Vick)
  5. [Pharmacy Information System](https://github.com/VictorOmondi1997/PharmacyInformationSystem)
* Graphic Designing
  * :flushed: OMG! Have you seen some of my great graphical designs? Check Them Here :point_down:
  1. [The End Is Here](https://www.behance.net/gallery/81480869/The-End-is-Here)
  2. [CodeLn Recruiters! We've Got The Compass](https://www.behance.net/gallery/81479901/CodeLn-Recruiters-Weve-Got-The-Compass)
  3. [Figma Sample](https://www.behance.net/gallery/81420927/Figma-Sample)
  4. [UI Quote](https://www.behance.net/gallery/81136861/UI_Quote)
  5. [Getting Started With FireBase](https://www.behance.net/gallery/81135301/Getting-Started-With-Firebase)
  6. [Vick Face](https://www.behance.net/gallery/80193757/Vick-Face)
* Watching Movies/Series
  * Here are Some Of the Movies I've Watched over the last period
  1. Always Be my may be.
  2. Lucifer Sn 1-2
  3. Marvels Runaways
  * And a lot more :flushed:
* Learning
  * Am Currently Enrolled in [Google Associate Android Development Curriculum](https://www.pluralsight.com)
  * and [FreeCodeCamp](https://www.freecodecamp.org)
