I am a _**BCT** student_ at **[Multimedia University of Kenya](https://www.mmu.ac.ke)** .
Due to my intrest in coding and being a team leader, I have been able to gain some leadership skills. :point_down:
* **[@CITClubMMU](https://www.citclubmmu.co.ke)** ***Assistant** Chairman*
* **[@CodeLN](https://www.codeln.com)** *Campus **Ambassador***
* **[@Moringa School](https://www.moringaschool.com)** *Campus **Ambassador***
* **[@DSC_MMu](https://www.twitter.com/DSC_MMU)** *Campus **Lead***

I am a **Web Developer**, **Graphic Designer**, **UI/UX Developer** and **Software Engineer**. My best *programming* Languages being **C**, **C++**, **Java** and **Python**. 

Check out my **[Github Account](https://www.github.com/VictorOmondi1997)** For more ***programming Languages*** that I've Used. Lets ***tweet*** on **[@VictorOmondi197](https://www.twitter.com/VictorOmondi197)**
