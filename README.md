# Victor Omondi

![Image of Victor Omondi](https://lh3.googleusercontent.com/eTKxQYfdDKqcpe2tdHYVc1k7GGnEIZwy67gXdoYpR8MpvGq6aTusg_8oQjrT0JR8DQD4XTE7LhY29G4SFNvS0JWSxEGZzvswKde-fv0mn3TQ6iuIP6RI3uUciCtsHm3P1ZwjaHccdM2d8k1czikWsdwTKZTBamkj8QBaMPbOzwXeMo02e3vavV6QmTleZNli4hj51ajKcbslI5rK49gCfIQcSX2v6zwE2PunDi6-W-VuTZBB-9VlsGlgX04bElx7yg6z4CQUDA3ML663Nh7Jt01rR9BixcsZVcNZ6_cR_bSv2qywNI_uqi-yPlpSj1eUQVm3tuyKVHrDKox4p-Bzt02pN4wdvNOKtQbcrABd4dcSabQ_Q3hA_u0xHtqZ9AJWnIZtob6MuQ4ZnnLuexKxS65_qgvon4qBnmxURENEz1XRUPW38jGn2nTDLfLFscG3d_ZP8yNGHy10m12FZL-EbGkjTuEHBiJSqJI8sNiccULZaOwc6LwAqQmeHSCdZeqFvFBnyJBmkhcrY3y2l48tF4XY_B74nH1hpCnWY-Y-x2YLCpwdtZsWq1aa5k6PBfqGajxafSDX3AKdqVZgKWnaYeQTm8ayL7xpl9T92Rqemcty_jx4yMQJh60-lGgTIA1Ph0oLPGT7XP_aClweQn0NarVgM-DR_pQ=w989-h655-no)

I am a _**BCT** student_ at **[Multimedia University of Kenya](https://www.mmu.ac.ke)** .
Due to my intrest in coding and being a team leader, I have been able to gain some leadership skills. :point_down:
* **[@CITClubMMU](https://www.citclubmmu.co.ke)** ***Assistant** Chairman*
* **[@CodeLN](https://www.codeln.com)** *Campus **Ambassador***
* **[@Moringa School](https://www.moringaschool.com)** *Campus **Ambassador***
* **[@DSC_MMu](https://www.twitter.com/DSC_MMU)** *Campus **Lead***

I am a **Web Developer**, **Graphic Designer**, **UI/UX Developer** and **Software Engineer**. My best *programming* Languages being **C**, **C++**, **Java** and **Python**. 

Check out my **[Github Account](https://www.github.com/VictorOmondi1997)** For more ***programming Languages*** that I've Used. Lets ***tweet*** on **[@VictorOmondi197](https://www.twitter.com/VictorOmondi197)**


[Github](https://github.com/VictorOmondi1997) [Twitter](https://www.twitter.com/VictorOmondi197) [LinkedIN](https://www.linkedin.com/in/VictorOmondi1997) [FaceBook](https://www.facebook.com/VictorOmondi1997) [Instagram](https://www.instagram.com/VictorOmondi1997) [Behance](https://www.behance.net/VictorOmondi1997) [Telegram](https://t.me/VickTheGeek) [Medium](https://www.medium.com/@VictorOmondi1997) [Website](https://victoromondi1997.netlify.com) 

* Programming/Coding
  * Currently, These are the repositories am working on.:point_down:
  1. [Markdown Proforlio](https://github.com/VictorOmondi1997/markdown-portfolio)
  2. [Tutomalix Education Website](https://github.com/VictorOmondi1997/TutomalixEducation-Website)
  3. [Vick Ventures](https://github.com/VictorOmondi1997/Web_Design_Practicals)
  4. [Victor Omondi Website](https://github.com/VictorOmondi1997/Vick)
  5. [Pharmacy Information System](https://github.com/VictorOmondi1997/PharmacyInformationSystem)
* Graphic Designing
  * :flushed: OMG! Have you seen some of my great graphical designs? Check Them Here :point_down:
  1. [The End Is Here](https://www.behance.net/gallery/81480869/The-End-is-Here)
  2. [CodeLn Recruiters! We've Got The Compass](https://www.behance.net/gallery/81479901/CodeLn-Recruiters-Weve-Got-The-Compass)
  3. [Figma Sample](https://www.behance.net/gallery/81420927/Figma-Sample)
  4. [UI Quote](https://www.behance.net/gallery/81136861/UI_Quote)
  5. [Getting Started With FireBase](https://www.behance.net/gallery/81135301/Getting-Started-With-Firebase)
  6. [Vick Face](https://www.behance.net/gallery/80193757/Vick-Face)
* Watching Movies/Series
  * Here are Some Of the Movies I've Watched over the last period
  1. Always Be my may be.
  2. Lucifer Sn 1-2
  3. Marvels Runaways
  * And a lot more :flushed:
* Learning
  * Am Currently Enrolled in [Google Associate Android Development Curriculum](https://www.pluralsight.com)
  * and [FreeCodeCamp](https://www.freecodecamp.org)


## Your GitHub Learning Lab Repository for Communicating Using Markdown

Welcome to **your** repository for your GitHub Learning Lab course. This repository will be used during the different activities that I will be guiding you through.

Oh! I haven't introduced myself...

I'm the GitHub Learning Lab bot and I'm here to help guide you in your journey to learn and master the various topics covered in this course. I will be using Issue and Pull Request comments to communicate with you. In fact, I already added an issue for you to check out.

![issue tab](https://lab.github.com/public/images/issue_tab.png)

I'll meet you over there, can't wait to get started!
